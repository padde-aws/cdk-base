import cdk = require('@aws-cdk/cdk');
import { Base } from './Base';

export interface AppProps {
    ApplicationName?: string;
    Environment?: string;
}

export class App extends cdk.App {
    private _values: AppProps;

    constructor(props?: AppProps) {
        super();

        // First, lets have a look at Environment Variables
        this.checkEnvironmentals();

        // Lets check the Context
        this.checkContext();

        // Last but not least: Check Properties
        this.checkProperties(props);

        Base.SetApplication(this);
    }

    private checkEnvironmentals() {
        if (process.env.ApplicationName) {
            this._ApplicationName = '' + process.env.ApplicationName;
        }
        if (process.env.Environment) {
            this._Environment = '' + process.env.Environment;
        }
    }

    private checkContext() {
        if (this.getContext('Environment')) {
            this._Environment = this.getContext('Environment');
        }
    }

    private checkProperties(props?: AppProps) {
        // this.addInfo('Checking Properties')
        if (props) {
            if (props.ApplicationName) {
                // this.addInfo('Propertie ApplicationName found: ' + props.ApplicationName)
                this._ApplicationName = props.ApplicationName;
            }
            if (props.Environment) {
                // this.addInfo('Propertie Environment found: ' + props.Environment)
                this._Environment = props.Environment;
            }
        }
    }

    // set the ApplicationName (must match regex '^[a-z]{0,16}$')
    private set _ApplicationName(value: string) {
        if (new RegExp('^[a-z]{0,16}$').test(value)) {
            this._values.ApplicationName = value;
        } else {
            throw new Error('ApplicationName ' + value + ' not valid, must match [a-z]{0,16}');
        }
    }

    // set the TechName (must match regex '^(prd|iat|tst|mro|trn|dev|nil)$')
    private set _Environment(value: string) {
        if (new RegExp('^(prd|iat|tst|mro|trn|dev|nil)$').test(value)) {
            this._values.Environment = value;
        } else {
            throw new Error('Environment ' + value + ' not valid, must match (prd|iat|tst|mro|trn|dev|nil)');
        }
    }

    private get _ApplicationName(): string {
        if (!this._values.ApplicationName) {
            throw new Error('ApplicationName not set');
        } else {
            return this._values.ApplicationName;
        }
    }

    public get ApplicationName(): string {
        return this._ApplicationName;
    }

    private get _Environment(): string {
        if (!this._values.Environment) {
            throw new Error('Environment not set');
        } else {
            return this._values.Environment;
        }
    }

    public get Environment(): string {
        return this._Environment;
    }
}
