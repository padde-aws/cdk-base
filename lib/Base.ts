import { App } from './App';

export namespace Base {
    let Application: App;

    export function GetApplication(): App {
        return Application;
    }

    export function SetApplication(app: App) {
        Application = app;
    }
}
